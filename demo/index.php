<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Src\VideoParser;

$import = null;

if (isset($argv[1])) {
    $import = $argv[1];
}

if (isset($_GET['import'])) {
    $import = $_GET['import'];
}

if ($import !== null) {
    $downloader = new \Src\Service\Downloader\VideoDownloader();
    $model      = new \Src\Service\Model\Video();


    $videoParser = new \Src\Service\Parser\VideoParser($model, $import);
    $videoParser->parse($downloader);
}
