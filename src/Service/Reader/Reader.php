<?php

namespace Src\Service\Reader;

class Reader
{
    public $filePath = '';

    public function __construct(string $filePath)
    {
        $this->filePath .= $filePath;
    }

    /**
     * @return array
     */
    public function read(): string
    {
        return file_get_contents($this->filePath);
    }
}