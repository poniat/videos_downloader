<?php

namespace src\Service\Downloader;

use Src\Service\Traits\CliHelper;
use Src\Service\Interfaces\VideoModelInterface as Video;

class VideoDownloader
{
    protected function getMessageTemplate(?string $title, ?string $url, ?string $tags, string $br = PHP_EOL): string
    {
        return sprintf('importing: "%s"; Url: %s; Tags: %s %s', $title, $url, $tags, $br);
    }

    public function download(array $videos = [])
    {
        $isCli = CliHelper::isCli();
        $nl    = $isCli ? PHP_EOL : '<br>';

        foreach ($videos as $video) {
            echo $this->getMessageTemplate($video->getTitle(), $video->getUrl(), $video->getTags(), $nl);

            // TODO importing a video from provided URL
        }
    }
}