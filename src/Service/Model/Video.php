<?php

namespace Src\Service\Model;

use Src\Service\Interfaces\VideoModelInterface;

class Video implements VideoModelInterface
{
    private $url;

    private $title;

    private $tags;

    public function setUrl(string $url): VideoModelInterface
    {
        $this->url = $url;

        return $this;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function setTitle(string $title): VideoModelInterface
    {
        $this->title = $title;

        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTags(string $tags): VideoModelInterface
    {
        $this->tags = $tags;

        return $this;
    }

    public function getTags(): string
    {
        return $this->tags;
    }
}