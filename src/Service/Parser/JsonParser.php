<?php

namespace Src\Service\Parser;

use Src\Service\Interfaces\ParserInterface;
use Src\Service\Interfaces\VideoModelInterface as Video;

class JsonParser implements ParserInterface
{
    public function parse(Video $video, string $input): array
    {
        $json = json_decode($input, true);

        if ($json === null || json_last_error() !== JSON_ERROR_NONE) {
            return [];
        }

        if(!array_key_exists('videos', $json)) {
            return [];
        }

        $results = [];

        foreach ($json['videos'] as $row) {
            $videoObj        = clone $video;
            $videoObj->url   = $row['url'];
            $videoObj->title = $row['title'];
            $videoObj->tags  = implode(',', $row['tags']);

            $results[] = $videoObj;
        }

        return $results;
    }
}