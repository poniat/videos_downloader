<?php

namespace Src\Service\Parser;

use Exception;
use Src\Service\Interfaces\VideoModelInterface as Model;
use Src\Service\Interfaces\DownloaderInterface as Downloader;

class VideoParser
{
    private $providerName = '';
    private $strategy = null;
    private $extension = '';

    public $model;

    public function __construct(Model $model, string $providerName)
    {
        $this->providerName = $providerName;
        $this->model        = $model;

        switch ($providerName) {
            case 'glorf':
                $this->strategy  = new JsonParser();
                $this->extension = '.json';
                break;
            case 'flub':
                $this->strategy  = new YamlParser();
                $this->extension = '.yaml';
                break;
            default:
                echo 'This extension is not supported!';
        }
    }

    public function parse(Downloader $downloader)
    {
        $fileName = $this->providerName . $this->extension;
        $filePath = dirname(__FILE__ , 4)
            . DIRECTORY_SEPARATOR
            . 'demo'
            . DIRECTORY_SEPARATOR
            . 'feed-exports'
            . DIRECTORY_SEPARATOR
            . $fileName;

        $reader = new \Src\Service\Reader\Reader($filePath);
        $input  = $reader->read();
        $videos = $this->strategy->parse($this->model, $input);

        $downloader->download($videos);
    }
}