<?php

namespace Src\Service\Parser;

use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;
use Src\Service\Interfaces\ParserInterface;
use Src\Service\Interfaces\VideoModelInterface as Video;

class YamlParser implements ParserInterface
{
    public function parse(Video $video, string $input): array
    {
        try {
            $data = Yaml::parse($input);
        } catch (ParseException $ex) {
            throw new Exception('Unable to parse the YAML string: %s', $ex->getMessage());
        }

        $results = [];

        foreach ($data as $row) {
            $videoObj        = clone $video;
            $videoObj->url   = $row['url'];
            $videoObj->title = $row['name'];

            if(array_key_exists('labels', $row) && $row['labels'] !== null) {
                $videoObj->tags = $row['labels'];
            }

            $results[] = $videoObj;
        }

        return $results;
    }
}