<?php

namespace Src\Service\Traits;

trait CliHelper
{
    public static function isCli(): bool
    {
        return (php_sapi_name() === 'cli');
    }
}