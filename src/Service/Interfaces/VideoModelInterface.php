<?php

namespace Src\Service\Interfaces;

interface VideoModelInterface
{
    public function setUrl(string $url): self;
    public function getUrl(): string;
    public function setTitle(string $title): self;
    public function getTitle(): string;
    public function setTags(string $tags): self;
    public function getTags(): string;
}