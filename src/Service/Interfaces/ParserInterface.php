<?php

namespace Src\Service\Interfaces;

use Src\Service\Interfaces\VideoModelInterface as Video;

interface ParserInterface
{
    public function parse(Video $video, string $input): array;
}