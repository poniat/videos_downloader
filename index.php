<?php

require_once __DIR__ . '/vendor/autoload.php';

use Src\VideoParser;
use Src\VideoDownloader;
use Src\Service\Traits\CliHelper;

$import = null;

if(isset($argv[1])) {
    $import = $argv[1];
}

if(isset($_GET['import'])) {
    $import = $_GET['import'];
}

if($import !== null) {
    $parser = new VideoParser($import);
    $downloader = new VideoDownloader();

    $videos = $parser->run();
    $downloader->run($videos);
}

