## Video downloader
 
Lib based on PHP in version 7.3.

At this moment a lib provides basic video downloader solution.
 
## Features
Extend a lib to download export files from SFTP and download videos from the provided URL's.

####Project provides:
- Classes which give you an simple example of problem solving solution for downloading video by provided URL.
- CMD output of downloading video process.
- PHPDoc.
- PHPUnit tests.
**Document checking completed. No errors or warnings to show.**

#### Installation steps:
1. Add PHP lib into your project and enjoy it.
2. Install a composer into main directory of the project. (`composer install` or `composer update`).

#### How to test the code:
1. In order to test application run fallowing commands:
- `phpunit` - for phpunit test.

#### How to run the code:
1. In order to run a demo fight in CMD, please go to /demo directory and run `php index.php`. 
Please, use `flub` or `glorf` to get success output. 

#### See working test below:
**[Test me live!](http://videodownloader.gcoders.co.uk/?import=flub)**
